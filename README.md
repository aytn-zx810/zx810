## Required APK
Termux - Terminal (com.termux_72.apk)

Termux API - API (com.termux.api_33.apk)

Termux Boot - Run server on boot up (com.termux.boot_5.apk)

## Install required APK

Open up TC and drag APK in one by one.

When drag and drop TC will prompt to install , choose phone as the installation target.

## ALL APK Installed 

Once all apk installed , lauch the Termux Boot app once to setup boot up, then close .

Note: you only need to run Termux Boot once.

Now open up Termux and run following commands .

### Install Termux API
`pkg install termux-api`

### Install Git to clone the ZX810 server code from Gitlab
`pkg install git`

### Clone the ZX910 repo
`git clone https://gitlab.com/aytn-zx810/zx810.git`

### Now setup directory to run the ZX810 Server
`mkdir ~/.termux`
`mkdir ~/.termux/boot`

### Now Copy the Server to the boot directory
`cp /zx810/server ~/.termux/boot`

### Now Reboot the ZX810

Either reboot manually or run following reboot command from Termux Terminal .

`/system/bin/reboot`

### Running

Look at the Android Notification bar and see if the Termux icon is present, if no icon the server is not runnig .

Try rebooting the ZX810.

### Troubleshooting 

If the server stop unexpectedly or it dosent run , make sure location is turmned on in the ZX810.

Go to 'Settings/Location' and make sure its turned on.

### Accessing the ZX810 API

**Note**: Replace "ZX810 IP Address" with the IP Address of the ZX810.

**http://"ZX810 IP Address":3000/api/v1/all** = Get All Data in JSON format.

**http://"ZX810 IP Address":3000/api/v1/location** = Get Location data in JSON format.

**http://"ZX810 IP Address":3000/api/v1/battery** = Get All battery Data in JSON format.

**http://"ZX810 IP Address":3000/api/v1/info** = Get All Phone Info Data in JSON format.

**http://"ZX810 IP Address":3000/api/v1/reboot** = Reboot Tracker.
