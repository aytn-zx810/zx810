package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
)

// Battery - Device Battery
type Battery struct {
	Health      string  `json:"health"`
	Percentage  int     `json:"percentage"`
	Status      string  `jso:"status"`
	Temperature float64 `json:"temperature"`
}

// Location - Device Location
type Location struct {
	Latitude   float64 `json:"latitude"`
	Longtitude float64 `json:"longtitude"`
	Altitude   float64 `json:"altitude"`
	Accuracy   float64 `json:"accuracy"`
	Bearing    float64 `json:"bearing"`
	Speed      float64 `json:"speed"`
	ElapsedMs  float64 `'json:"elapsedms"`
	Provider   string  `json:"provider"`
}

// Deviceinfo - Device info
type Deviceinfo struct {
	DataActivity          string `json:"data_activity"`
	DataState             string `json:"data_state"`
	DeviceID              string `json:"device_id"`
	DeviceSoftwareVersion string `json:"device_software_version"`
	PhoneCount            int    `json:"phone_count"`
	PhoneType             string `json:"phone_type"`
	NetworkOperator       string `json:"network_operator"`
	NetworkOperatorName   string `json:"network_operator_name"`
	NetworkCountryIso     string `json:"network_county_iso"`
	NetworkType           string `json:"network_type"`
	NetworkRoaming        bool   `json:"network_roaming"`
	SimCountryIso         string `json:"sim_country_iso"`
	SimOperator           string `json:"sim_operator"`
	SimOperatorName       string `json:"sim_operator_name"`
	SimSerialNumber       string `json:"sim_serial_number"`
	SimState              string `json:"sim_state"`
}

// DeviceDataPayload - All Device Data Combined.
type DeviceDataPayload struct {
	ID string `json:"id"`
	// Battery
	Health      string  `json:"health"`
	Percentage  float64 `json:"percentage"`
	Plugged     string  `json:"plugged"`
	Status      string  `jso:"status"`
	Temperature float64 `json:"temperature"`
	// Location
	Latitude   float64 `json:"latitude"`
	Longtitude float64 `json:"longitude"`
	// Altitude   float64 `json:"altitude"`
	// Accuracy   float64 `json:"accuracy"`
	// Bearing    float64 `json:"bearing"`
	// Speed      float64 `json:"speed"`
	// ElapsedMs  float64 `'json:"elapsedms"`
	// Provider   string  `json:"provider"`
	// Info
	// DataActivity          string  `json:"data_activity"`
	// DataState             string  `json:"data_state"`
	DeviceID string `json:"device_id"`
	// DeviceSoftwareVersion string  `json:"device_software_version"`
	// PhoneCount            float64 `json:"phone_count"`
	// PhoneType             string  `json:"phone_type"`
	// NetworkOperator       string  `json:"network_operator"`
	// NetworkOperatorName   string  `json:"network_operator_name"`
	// NetworkCountryIso     string  `json:"network_county_iso"`
	// NetworkType           string  `json:"network_type"`
	// NetworkRoaming        bool    `json:"network_roaming"`
	// SimCountryIso         string  `json:"sim_country_iso"`
	// SimOperator           string  `json:"sim_operator"`
	// SimOperatorName       string  `json:"sim_operator_name"`
	SimSerialNumber string `json:"sim_serial_number"`
	// SimSubscriberID       string  `json:"sim_subscriber_id"`
	// SimState              string  `json:"sim_state"`
	// IP
	DeviceIP string `json:"device_ip"`
	// Timestamp
	Timestamp int32 `json:"datetime"`
}

func init() {
	go wakeLock()
	go periodic()
	go startPeriodic()
}

// Send data periodicly every 10 minutes .
func startPeriodic() {
	for {
		<-time.After(60 * time.Second)
		go periodic()
	}
}

func wakeLock() {
	// termux-wake-lock
	_, err := exec.Command("termux-wake-lock").Output()
	if err != nil {
		log.Print(err)
	}
}

func main() {
	fmt.Println("Starting AYTN Server Up..")
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	r := chi.NewRouter()
	// Middleware
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(cors.Handler)
	r.Get("/api/v1/location", GetLocation)
	r.Get("/api/v1/reboot", RebootTracker)
	r.Get("/api/v1/battery", GetBattery)
	r.Get("/api/v1/info", DeviceInfo)
	r.Get("/api/v1/all", GetAll) // Change to Post later
	fmt.Println("AYTN Micro Server Running...")
	http.ListenAndServe(":3000", r)

}

// Reboot Tracker
func RebootTracker(w http.ResponseWriter, r *http.Request) {
	command := exec.Command("/system/bin/reboot")

	// set var to get the output
	var out bytes.Buffer

	// set the output to our variable
	command.Stdout = &out

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Tracker going down.."))

	err := command.Run()
	if err != nil {
		log.Println(err)
	}
}

// GetLocation - Return device GPS coordinates using Termux API
func GetLocation(w http.ResponseWriter, r *http.Request) {
	cmd, err := exec.Command("termux-location", "-p", "gps", "-r", "once").Output()
	if err != nil {
		log.Print(err)
	}

	var location Location
	json.Unmarshal(cmd, &location)

	// fmt.Printf("%s", cmd)
	fmt.Fprintf(w, string(cmd))

}

// GetBattery - Retrieve battery infomation of the Device using Termux API
func GetBattery(w http.ResponseWriter, r *http.Request) {
	cmd, err := exec.Command("termux-battery-status").Output()
	if err != nil {
		log.Print(err)
	}

	// fmt.Printf("%s", cmd)
	fmt.Fprintf(w, string(cmd))
}

// DeviceInfo - Get DeviceInfo using Termux API
func DeviceInfo(w http.ResponseWriter, r *http.Request) {
	cmd, err := exec.Command("termux-telephony-deviceinfo").Output()
	if err != nil {
		log.Print(err)
	}
	var battery Battery
	json.Unmarshal(cmd, &battery)
	// fmt.Printf("%s", cmd)
	fmt.Fprintf(w, string(cmd))
}

// GetAll - Receive POST and return all Location,Battery,Info,timestamp,IP
func GetAll(w http.ResponseWriter, r *http.Request) {
	log.Println("GetAll Payload..")
	location := GetLocationApi()

	var locationData map[string]interface{}

	if err := json.Unmarshal(location, &locationData); err != nil {
		log.Print("Getall: Failed to Unmarshal Location Data")

	}
	battery := GetBatteryApi()

	var batteryData map[string]interface{}

	if err := json.Unmarshal(battery, &batteryData); err != nil {
		log.Print("Getall: Failed to Unmarshal Battery Data")
	}
	deviceInfo := DeviceInfoApi()

	var deviceInfoData map[string]interface{}

	if err := json.Unmarshal(deviceInfo, &deviceInfoData); err != nil {
		log.Print("Failed to Unmarshal Device info Data")
	}

	//fmt.Println(deviceInfoData)

	DeviceData := &DeviceDataPayload{
		ID:          deviceInfoData["sim_serial_number"].(string),
		Health:      batteryData["health"].(string),
		Percentage:  batteryData["percentage"].(float64),
		Plugged:     batteryData["plugged"].(string),
		Status:      batteryData["status"].(string),
		Temperature: batteryData["temperature"].(float64),
		// Location
		Latitude:        locationData["latitude"].(float64),
		Longtitude:      locationData["longitude"].(float64),
		DeviceID:        deviceInfoData["device_id"].(string),
		SimSerialNumber: deviceInfoData["sim_serial_number"].(string),
		DeviceIP:        getIP(),
		// TimeStamp
		Timestamp: int32(time.Now().Unix()),
	}

	dataJSON, err := json.Marshal(DeviceData)
	if err != nil {
		log.Print("Failed to Marshal dataJSON Data")
	}
	fmt.Println(DeviceData)
	w.Header().Set("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(dataJSON)
}

// RebootDevice - Reboot the Device
func RebootDevice() {
	return
}

// GetLocationApi - Return device GPS coordinates using Termux API
func GetLocationApi() []byte {
	cmd, err := exec.Command("termux-location", "-p", "gps", "-r", "once").Output()
	if err != nil {
		log.Print("GetLocationApi: Failed to get Location")
		//return nil, err
	}

	//fmt.Printf("%s", cmd)
	return cmd

}

// GetBatteryApi - Retrieve battery infomation
func GetBatteryApi() []byte {
	cmd, err := exec.Command("termux-battery-status").Output()
	if err != nil {
		log.Print("GetBatteryApi: Failed to get Battery")
		//return nil, err
	}

	// fmt.Printf("%s", cmd)
	return cmd
}

// DeviceInfoAPI - Get DeviceInfo using Termux API
func DeviceInfoApi() []byte {
	cmd, err := exec.Command("termux-telephony-deviceinfo").Output()
	if err != nil {
		log.Print("DeviceInfoApi: Failed to get DeviceInfo")
		//return nil, err
	}
	// var battery Battery
	// json.Unmarshal(cmd, &battery)
	// fmt.Printf("%s", cmd)
	return cmd
}

/// HELPER FUNCTIONS ///

func getIP() string {
	addrs, err := net.InterfaceAddrs()
	var ip string
	if err != nil {
		os.Stderr.WriteString("Oops: " + err.Error() + "\n")
		os.Exit(1)
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ip = ipnet.IP.String()
			}
		}
	}
	return ip
}

// Push Data on a periodic interval
func periodic() {
	log.Print("Periodic Inform Triggered")
	location := GetLocationApi()
	battery := GetBatteryApi()
	deviceInfo := DeviceInfoApi()

	var locationData map[string]interface{}
	var batteryData map[string]interface{}
	var deviceInfoData map[string]interface{}

	// Should really handle each case individually @ location, batteryData, devieInfodata
	if location == nil {
		log.Print("Periodic: failed to get Location")
	} else {
		if errML := json.Unmarshal(location, &locationData); errML != nil {
			log.Print("Periodic: Failed to Unmarshal Location Data")
			//return errML
		}
	}
	if battery == nil && deviceInfo == nil {
		log.Print("Periodic: Failed to get device Resource")
	} else {

		if errMB := json.Unmarshal(battery, &batteryData); errMB != nil {
			log.Print("Periodic: Failed to Unmarshal Battery Data")
			//return errMB
		}

		if errMD := json.Unmarshal(deviceInfo, &deviceInfoData); errMD != nil {
			log.Print("Periodic: Failed to Unmarshal Device info Data")
			// return errMD
		}

		//fmt.Println(deviceInfoData)

		DeviceData := &DeviceDataPayload{
			ID:          deviceInfoData["sim_serial_number"].(string),
			Health:      batteryData["health"].(string),
			Percentage:  batteryData["percentage"].(float64),
			Plugged:     batteryData["plugged"].(string),
			Status:      batteryData["status"].(string),
			Temperature: batteryData["temperature"].(float64),
			// Location
			Latitude:        locationData["latitude"].(float64),
			Longtitude:      locationData["longitude"].(float64),
			DeviceID:        deviceInfoData["device_id"].(string),
			SimSerialNumber: deviceInfoData["sim_serial_number"].(string),
			// IP
			DeviceIP: getIP(),
			// TimeStamp
			Timestamp: int32(time.Now().Unix()),
		}

		dataJSON, errDData := json.Marshal(DeviceData)
		if errDData != nil {
			log.Print("Periodic: Failed to Marshal dataJSON Data")
			// return errDData
		}

		callAPIPOST(dataJSON)
	}

}

func callAPIPOST(payload []byte) []byte {
	APIURL := "https://api.aytn.com.au/api/v1/periodic"
	//fmt.Println("Periodic Inform Invoked..")
	req, err := http.NewRequest("POST", APIURL, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json; param=value") // This makes it work
	if err != nil {
		panic(err)
	}
	client := http.DefaultClient
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	return body
}
